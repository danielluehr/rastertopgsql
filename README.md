# raster2pgsql (QGIS plugin) #



### Summary ###

* QGIS plugin UI to call external raster2pgsql program in order to upload a raster to a PostGIS enabled DB.
* Version 0.1
* Tested with QGIS 2.8.9-Wien and PostGIS/raster2pgsql 2.1.4 GDAL_VERSION=111 (r12966)

It currently saves raster2pgsql output to SQL file to be injected "manually" to the DB.

### TODO ###

* Documentation.
* i18n.
* Add missing raster2pgsql's option to UI and implement the parsing.
* Add code to send the SQL directly to DB (it would be nice if the code eventually became part of dbmanager).
* Clean up code and add more error checking.
* Tidy up the interface.
* Consider other versions of raster2pgsql.

### Contribution guidelines ###

* Writing tests
* Code review
* Translations