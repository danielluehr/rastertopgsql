# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RasterToPgsql
                                 A QGIS plugin
 Raster to pgsql plugin calls raster2pgsql to upload rasters to a PostGIS DB.
                              -------------------
        begin                : 2016-06-15
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Daniel Vicente Lühr Sierra
        email                : dluhr@ieee.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QFileDialog
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from raster_to_pgsql_dialog import RasterToPgsqlDialog
import os.path
from qgis.gui import QgsMessageBar
from qgis.core import QgsCoordinateReferenceSystem
import shlex
import subprocess
import gdal
import osr
from gdalconst import *


class RasterToPgsql:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        # plugin-builder generates ??.qm files. locale_path should know this.
        # locale_path = os.path.join(
        #     self.plugin_dir,
        #     'i18n',
        #     'RasterToPgsql_{}.qm'.format(locale))
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            '{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = RasterToPgsqlDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Raster to PostgreSQL')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'RasterToPgsql')
        self.toolbar.setObjectName(u'RasterToPgsql')

        self.dlg.lineEditRasterFile.clear()
        self.dlg.pushButtonRasterFile.clicked.connect(self.select_raster_file)
        self.dlg.pushButtonSqlFile.clicked.connect(self.select_sql_file)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('RasterToPgsql', message)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToRasterMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/RasterToPgsql/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Raster to PostgreSQL'),
            callback=self.run,
            parent=self.iface.mainWindow())

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginRasterMenu(
                self.tr(u'&Raster to PostgreSQL'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def select_raster_file(self):
        msg = self.tr("Select raster file(s) ")
        filename = QFileDialog.getOpenFileName(self.dlg, msg, "", '')
        self.dlg.lineEditRasterFile.setText(filename)
        # Use gdal and 'friends' to count bands and to get the raster's srid.
        # Use this values to populate the widgets accordingly.
        rasterDataset = gdal.Open(filename, GA_ReadOnly)
        if rasterDataset is None:
            title = self.tr("Warning")
            msg = self.tr("Cannot read raster file. Please, "
                          + "check its integrity or select another one.")
            lvl = QgsMessageBar.WARNING
            self.iface.messageBar().pushMessage(tit, msg, level=lvl)
        else:
            gdalProjection = rasterDataset.GetProjection()
            gdalBands = rasterDataset.RasterCount
            # Process bands
            self.dlg.listWidgetBands.clear()
            for band in range(gdalBands):
                self.dlg.listWidgetBands.insertItem(band, str(band + 1))
            firstBand = self.dlg.listWidgetBands.item(0)
            self.dlg.listWidgetBands.setItemSelected(firstBand, True)
            # Process projection data.
            tempProj = osr.SpatialReference()
            tempProj.ImportFromWkt(gdalProjection)
            qgisProj = QgsCoordinateReferenceSystem()
            qgisProj.createFromProj4(tempProj.ExportToProj4())
            # Set SRID selector with the qgisProj value.
            self.dlg.mQgsProj.setCrs(qgisProj)

    def select_sql_file(self):
        msg = self.tr("Select sql file")
        fileFilter = self.tr('SQL files (*.sql)')
        filename = QFileDialog.getSaveFileName(self.dlg, msg, "", fileFilter)
        self.dlg.lineEditSqlFile.setText(filename)

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.

            # raster2pgsql options string
            r2pOpts = ' '
            # check table access mode options
            if self.dlg.radioButtonCreate.isChecked():
                r2pOpts += '-c '
            elif self.dlg.radioButtonAppend.isChecked():
                r2pOpts += '-a '
            elif self.dlg.radioButtonDrop.isChecked():
                r2pOpts += '-d '
            elif self.dlg.radioButtonPrepare.isChecked():
                r2pOpts += '-p '
            else:
                title = self.tr("Error")
                msg = self.tr("wrong access mode")
                lvl = QgsMessageBar.CRITICAL
                self.iface.messageBar().pushMessage(title, msg, level=lvl)
            # get SRID if specified
            if self.dlg.groupBoxSRID.isChecked():
                srid = self.dlg.mQgsProj.crs().postgisSrid()
                r2pOpts += '-s ' + str(srid) + ' '
            # Tile size
            if self.dlg.groupBoxTileSize.isChecked():
                r2pOpts += '-t '
                if self.dlg.groupBoxTileSizeSpecify.isChecked():
                    tileWidth = self.dlg.spinBoxWidth.text()
                    tileHeight = self.dlg.spinBoxHeight.text()
                    tileSize = tileWidth + 'x' + tileHeight
                else:
                    tileSize = 'auto'
                r2pOpts += tileSize + ' '
            # Constraints
            if self.dlg.groupBoxConstraints.isChecked():
                r2pOpts += '-C '
                if self.dlg.checkBoxDisableMaxExtent.isChecked():
                    r2pOpts += '-x '
                if self.dlg.checkBoxRegularBlocking.isChecked():
                    r2pOpts += '-r '
            # Bands
            if self.dlg.groupBoxBands.isChecked():
                r2pOpts += '-b '
                selectedItems = self.dlg.listWidgetBands.selectedItems()
                labels = list()
                for item in selectedItems:
                    labels.append(item.text())
                r2pOpts += ','.join(labels) + ' '
            # Optional parameters
            if self.dlg.checkBoxQuotes.isChecked():
                r2pOpts += '-q '
            if self.dlg.checkBoxFilename.isChecked():
                r2pOpts += '-F '
            if self.dlg.checkBoxGist.isChecked():
                r2pOpts += '-I '
            if self.dlg.checkBoxVacuum.isChecked():
                r2pOpts += '-M '
            if self.dlg.lineEditRasterFile.hasAcceptableInput():
                rasterFile = self.dlg.lineEditRasterFile.text()
                r2pOpts += rasterFile + ' '
            else:
                title = self.tr("Error")
                msg = self.tr("Invalid raster filename.")
                lvl = QgsMessageBar.CRITICAL
                self.iface.messageBar().pushMessage(title, msg, level=lvl)
                return False  # Is this the best way to terminate the script?
            # TODO: ask for and process schemaName(or just keep public?)
            if self.dlg.lineEditTableName.hasAcceptableInput():
                tableName = self.dlg.lineEditTableName.text()
                schemaName = 'public'
                r2pOpts += schemaName + '.' + tableName
            # TODO: call/execute command  and send sql to DB
            # In the meantime let's just save a sql file for manual insertion.
            if self.dlg.lineEditSqlFile.hasAcceptableInput():
                sqlFileName = self.dlg.lineEditSqlFile.text()
                sqlFile = open(sqlFileName, 'w')
            else:
                title = self.tr("Error")
                msg = self.tr("Invalid sql filename.")
                lvl = QgsMessageBar.CRITICAL
                self.iface.messageBar().pushMessage(title, msg, level=lvl)
                return False  # Is this the best way to terminate the script?
            # Prepare command for execution
            r2pCommand = 'raster2pgsql ' + r2pOpts
            r2pArgs = shlex.split(r2pCommand)
            # Execute command. TODO: send output to DB instead.
            r2pReturn = subprocess.call(r2pArgs, stdout=sqlFile)
            if r2pReturn == 0:
                title = self.tr("Result")
                msg = self.tr("SQL file created: ") + sqlFileName
                lvl = QgsMessageBar.SUCCESS
                self.iface.messageBar().pushMessage(title, msg, level=lvl)
            else:
                title = self.tr("Error")
                msg = self.tr("Subprocess raster2pgsql failed.")
                lvl = QgsMessageBar.CRITICAL
                self.iface.messageBar().pushMessage(title, msg, level=lvl)
            sqlFile.close()
            # debug: show option string
            # title = "Debug"
            # msg1 = "r2pOpts: "+r2pOpts
            # msg2 = "command: "+r2pCommand
            # lvl = QgsMessageBar.INFO
            # self.iface.messageBar().pushMessage(title, msg1, level=lvl)
            # self.iface.messageBar().pushMessage(title, msg2, level=lvl)
            # pass
