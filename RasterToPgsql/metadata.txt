# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Raster to PostgreSQL
qgisMinimumVersion=2.8
description=The raster to pgsql plugin calls raster2pgsql to upload rasters to a PostGIS DB.
version=0.1
author=Daniel V. Lühr Sierra
email=dluhr@ieee.org

about=This plugins calls raster2pgsql command to upload raster data to a PostGIS enabled PostgreSQL DB.

tracker=https://bitbucket.org/danielluehr/rastertopgsql/issues?status=new&status=open
repository=https://bitbucket.org/danielluehr/rastertopgsql/overview
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=postgis, raster, database

homepage=https://bitbucket.org/danielluehr/rastertopgsql/overview
category=Raster
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

