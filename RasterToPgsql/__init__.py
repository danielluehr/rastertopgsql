# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RasterToPgsql
                                 A QGIS plugin
 Raster to pgsql plugin calls raster2pgsql to upload rasters to a PostGIS DB.
                             -------------------
        begin                : 2016-06-15
        copyright            : (C) 2016 by Daniel Vicente Lühr Sierra
        email                : dluhr@ieee.org
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load RasterToPgsql class from file RasterToPgsql.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .raster_to_pgsql import RasterToPgsql
    return RasterToPgsql(iface)
