<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>@default</name>
    <message>
        <location filename="test_translations.py" line="48"/>
        <source>Good morning</source>
        <translation>Buenos días</translation>
    </message>
</context>
<context>
    <name>RasterToPgsql</name>
    <message>
        <location filename="raster_to_pgsql.py" line="182"/>
        <source>&amp;Raster to PostgreSQL</source>
        <translation>&amp;Ráster a PostgreSQL</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="172"/>
        <source>Raster to PostgreSQL</source>
        <translation>Ráster a PostgreSQL</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="190"/>
        <source>Select raster file(s) </source>
        <translation>Seleccione el(los) archivo(s) ráster </translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="195"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="195"/>
        <source>Cannot read raster file. Please, check its integrity or select another one.</source>
        <translation>No se puede leer el archivo ráster. Por favor, revise su integridad o seleccione otro.</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="213"/>
        <source>Select sql file</source>
        <translation>Seleccione archivo SQL</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="213"/>
        <source>SQL files (*.sql)</source>
        <translation>Archivos SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="305"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="239"/>
        <source>wrong access mode</source>
        <translation>modo de acceso erróneo</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="282"/>
        <source>Invalid raster filename.</source>
        <translation>Nombre de archivo ráster inválido.</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="295"/>
        <source>Invalid sql filename.</source>
        <translation>Nombre de archivo SQL inválido.</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="303"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="303"/>
        <source>SQL file created: </source>
        <translation>Archivo SQL creado: </translation>
    </message>
    <message>
        <location filename="raster_to_pgsql.py" line="305"/>
        <source>Subprocess raster2pgsql failed.</source>
        <translation>Falló sub-proceso raster2pgsql.</translation>
    </message>
</context>
<context>
    <name>RasterToPgsqlDialogBase</name>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="14"/>
        <source>Raster to PostgreSQL</source>
        <translation>Ráster a PostgreSQL</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="42"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="66"/>
        <source>Create new table, then populate</source>
        <translation>Crear nueva tabla, luego llenar</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="82"/>
        <source>Append to existing table</source>
        <translation>Agregar a tabla existente</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="95"/>
        <source>Drop table, then create, populate</source>
        <translation>Borrar tabla, luego crear y llenar</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="108"/>
        <source>Prepare table (don&apos;t populate)</source>
        <translation>Preparar tabla (sin llenar)</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="122"/>
        <source>SRID</source>
        <translatorcomment>¿SRID in spanish?</translatorcomment>
        <translation>SRID</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="151"/>
        <source>Tile size</source>
        <translation>Tamaño de tesela</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="169"/>
        <source>Specify tile size</source>
        <translation>Especificar</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="190"/>
        <source>Width:</source>
        <translation>Ancho:</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="197"/>
        <source>Height</source>
        <translation>Alto</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="221"/>
        <source>Apply constraints</source>
        <translation>Aplicar restricciones</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="236"/>
        <source>Disable max extent constraint</source>
        <translation>Desabilitar restricción «Max Extent»</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="249"/>
        <source>Set regular blocking constraints</source>
        <translation>Activar restricción «Regular Blocking»</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="263"/>
        <source>Optional parameters</source>
        <translation>Parámetros opcionales</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="278"/>
        <source>Use quotes</source>
        <translation>Usar comillas</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="285"/>
        <source>Add filename</source>
        <translation>Agregar nombre de archivo</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="295"/>
        <source>Create GiST index</source>
        <translation>Crear índice GiST</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="305"/>
        <source>Vacuum analyze</source>
        <translatorcomment>¿nombre técnico en castellano?</translatorcomment>
        <translation>«Vacuum analyze»</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="328"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select raster file(s)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Seleccionar archivo(s) ráster&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="393"/>
        <source>. . .</source>
        <translation>. . .</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="357"/>
        <source>Table name</source>
        <translation>Nombre de tabla</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="364"/>
        <source>raster01</source>
        <translation>raster01</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;SQL output file&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Archivo SQL de salida&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="raster_to_pgsql_dialog_base.ui" line="409"/>
        <source>Bands</source>
        <translation>Bandas</translation>
    </message>
</context>
</TS>
